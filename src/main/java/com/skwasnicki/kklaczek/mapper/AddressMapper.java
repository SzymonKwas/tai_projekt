package com.skwasnicki.kklaczek.mapper;

import com.skwasnicki.kklaczek.domain.Address;
import com.skwasnicki.kklaczek.service.dto.AddressDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    AddressDTO addressEntityToDto(Address address);
    @Mapping(target ="id", expression = "java(addressDTO.getId())")
    Address addressDtoToEntity(AddressDTO addressDTO);
}
