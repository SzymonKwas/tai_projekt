package com.skwasnicki.kklaczek.mapper;

import com.skwasnicki.kklaczek.domain.DictionaryCity;
import com.skwasnicki.kklaczek.domain.DictionaryCountry;
import com.skwasnicki.kklaczek.domain.Person;
import com.skwasnicki.kklaczek.service.dto.DictionaryDTO;
import com.skwasnicki.kklaczek.service.dto.PersonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DictionaryMapper {
    DictionaryDTO dictionaryConutryToDictionaryDTO(DictionaryCountry dictionaryCountry);
    DictionaryDTO dictionaryCityToDictionaryDTO(DictionaryCity dictionaryCity);
}
