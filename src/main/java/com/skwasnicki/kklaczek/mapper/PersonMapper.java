package com.skwasnicki.kklaczek.mapper;

import com.skwasnicki.kklaczek.domain.Person;
import com.skwasnicki.kklaczek.service.dto.PersonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface PersonMapper {

    PersonDTO personEntityToDto(Person person);

    @Mapping(target ="id", expression = "java(personDTO.getId())")
    Person personDtoToEntity(PersonDTO personDTO);

}
