package com.skwasnicki.kklaczek.mapper;

import com.skwasnicki.kklaczek.domain.Authority;
import com.skwasnicki.kklaczek.domain.User;
import com.skwasnicki.kklaczek.service.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {PersonMapper.class, AddressMapper.class}, imports= {Collectors.class, Authority.class})
public interface UserMapper {
    @Mapping(target = "authorities", expression = "java(user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet()))")
    UserDTO userEntityToDto(User user);
}
