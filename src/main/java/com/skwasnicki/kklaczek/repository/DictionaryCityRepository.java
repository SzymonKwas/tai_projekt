package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.DictionaryCity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DictionaryCityRepository extends JpaRepository<DictionaryCity, Long> {
}
