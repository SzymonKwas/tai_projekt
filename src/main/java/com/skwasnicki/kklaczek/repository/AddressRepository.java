package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Long> {

}
