package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.Plane;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaneRepository extends JpaRepository<Plane,Long> {
}
