package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.Flight;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FlightRepository extends JpaRepository<Flight,Long> {

}
