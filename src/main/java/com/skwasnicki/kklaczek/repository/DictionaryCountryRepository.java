package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.DictionaryCountry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DictionaryCountryRepository extends JpaRepository<DictionaryCountry,Long> {
}
