package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.BookTicket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookTicketRepository extends JpaRepository<BookTicket,Long> {
}
