package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.Flight;
import com.skwasnicki.kklaczek.domain.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TicketRepository extends JpaRepository<Ticket,Long> {
    Optional<Ticket> findByFlight(Flight flight);

}
