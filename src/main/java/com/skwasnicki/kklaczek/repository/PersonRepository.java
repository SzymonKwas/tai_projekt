package com.skwasnicki.kklaczek.repository;

import com.skwasnicki.kklaczek.domain.Person;
import com.skwasnicki.kklaczek.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person,Long> {
}
