package com.skwasnicki.kklaczek.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

@Service
public class JacksonFileReaderService {

    @Autowired
    private ObjectMapper objectMapper;
    private final Logger log = LoggerFactory.getLogger(JacksonFileReaderService.class);

    public JacksonFileReaderService(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Map<String, Object> parseJsonFileToMap(String filePath) throws IOException {
        log.info("Mapping jsonFile to jsonMap {{}}", filePath);
        File file = ResourceUtils.getFile(filePath);
        Map<String, Object> jsonMap = objectMapper.readValue(file,
            new TypeReference<Map<String, String>>() {
            });
        log.info("Mapped jsonMap: {{}}}", jsonMap);
        return jsonMap;
    }

}
