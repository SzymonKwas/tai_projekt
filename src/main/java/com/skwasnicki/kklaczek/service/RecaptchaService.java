package com.skwasnicki.kklaczek.service;

import com.skwasnicki.kklaczek.config.RecaptchaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
@EnableConfigurationProperties(RecaptchaProperties.class)
public class RecaptchaService {

    private final RestTemplate restTemplate;

    private final Environment env;
    private String recaptchaSecret;
    private String recaptchaVerifyUrl;

    public RecaptchaService(RestTemplateBuilder restTemplateBuilder, Environment env) {
        this.restTemplate = restTemplateBuilder.build();
        this.env = env;

        recaptchaSecret = env.getProperty("recaptcha.secret-key");
        recaptchaVerifyUrl = env.getProperty("recaptcha.verification-url");
    }

    public boolean verify(String response) {
        MultiValueMap param = new LinkedMultiValueMap<>();
        param.add("secret", recaptchaSecret);
        param.add("response", response);

        RecaptchaResponse recaptchaResponse = null;
        try {
            recaptchaResponse = this.restTemplate.postForObject(recaptchaVerifyUrl, param, RecaptchaResponse.class);
        } catch (RestClientException e) {
            System.out.print(e.getMessage());
        }
        if (recaptchaResponse.isSuccess()) {
            return true;
        } else {
            return false;
        }
    }
}

