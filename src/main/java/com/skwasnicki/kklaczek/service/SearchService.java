package com.skwasnicki.kklaczek.service;

import com.skwasnicki.kklaczek.domain.Flight;
import com.skwasnicki.kklaczek.domain.Ticket;
import com.skwasnicki.kklaczek.service.dto.SearchDto;

import java.util.List;

public interface SearchService {
    List<Flight> getFlightBetween(SearchDto searchDto);
}
