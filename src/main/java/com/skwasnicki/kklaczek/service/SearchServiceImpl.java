package com.skwasnicki.kklaczek.service;

import com.skwasnicki.kklaczek.domain.Flight;
import com.skwasnicki.kklaczek.domain.types.City;
import com.skwasnicki.kklaczek.repository.FlightRepository;
import com.skwasnicki.kklaczek.service.dto.SearchDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchServiceImpl implements SearchService {

    private FlightRepository flightRepository;

    public SearchServiceImpl(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    @Override
    public List<Flight> getFlightBetween(SearchDto searchDto) {
        List<Flight> flightsList = flightRepository.findAll();

        //QueryDSL
        City fromCity = getValueOfCity(searchDto.getFromPlace());
        City toCity = getValueOfCity(searchDto.getToPlace());
        LocalDateTime fromDate = searchDto.getFromDate();
        LocalDateTime toDate = searchDto.getToDate();

        flightsList = flightsList.stream()
            .filter((fromCity != null) ? t -> t.getFromCity().equals(fromCity) : t -> true)
            .filter((toCity != null) ? t -> t.getToCity().equals(toCity) : t -> true)
            .filter((fromDate != null) ? t -> t.getDate().isAfter(fromDate) : t -> true)
            .filter((toDate != null) ? t -> t.getDate().isBefore(toDate) : t -> true)
            .collect(Collectors.toList());

        return flightsList;
    }

    private City getValueOfCity(String place) {
        return StringUtils.isEmpty(place) ? null : City.valueOf(place);
    }
}
