package com.skwasnicki.kklaczek.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
public class DictionaryService {

    private static String CITIES_DICTIONARY_PATH = "classpath:content/cities.json";
    private static String COUNTRIES_DICTIONARY_PATH = "classpath:content/countries.json";

    @Autowired
    private JacksonFileReaderService jacksonFileReaderService;

    public DictionaryService(JacksonFileReaderService jacksonFileReaderService) {
        this.jacksonFileReaderService = jacksonFileReaderService;
    }

    public Map<String, Object> getCitiesDictionary() throws IOException {
        return jacksonFileReaderService.parseJsonFileToMap(CITIES_DICTIONARY_PATH);
    }

    public Map<String, Object> getCountriesDictionary() throws IOException {
        return jacksonFileReaderService.parseJsonFileToMap(COUNTRIES_DICTIONARY_PATH);
    }
}

