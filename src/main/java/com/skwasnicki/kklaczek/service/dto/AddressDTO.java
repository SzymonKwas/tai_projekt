package com.skwasnicki.kklaczek.service.dto;

import com.skwasnicki.kklaczek.domain.Address;

public class AddressDTO {

    private Long id;
    private String country;
    private String postalCode;
    private String city;
    private String street;
    private String buildingNumber;
    private String flatNumber;

    public AddressDTO(Address address) {
        if (address != null) {
            this.id = address.getId();
            this.country = address.getCountry();
            this.postalCode = address.getPostalCode();
            this.city = address.getCity();
            this.street = address.getStreet();
            this.buildingNumber = address.getBuildingNumber();
            this.flatNumber = address.getFlatNumber();
        }
    }

    public AddressDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }
}
