package com.skwasnicki.kklaczek.service.dto;

import com.skwasnicki.kklaczek.domain.Person;
import com.skwasnicki.kklaczek.domain.types.AddressType;
import com.skwasnicki.kklaczek.domain.types.Sex;

import java.time.LocalDate;
import java.util.Map;

public class PersonDTO {
    private Long id;
    private AddressDTO address;
    private Sex sex;
    private String lastName;
    private String firstName;
    private String middleName;
    private LocalDate birthDate;
    private String pesel;
    private String phoneNumber;
    private String email;

    public PersonDTO() {
    }

    public PersonDTO(Person person) {
        if (person != null) {
            this.id = person.getId();
            this.address = new AddressDTO(person.getAddress());
            this.sex = person.getSex();
            this.lastName = person.getLastName();
            this.firstName = person.getFirstName();
            this.middleName = person.getMiddleName();
            this.birthDate = person.getBirthDate();
            this.pesel = person.getPesel();
            this.phoneNumber = person.getPhoneNumber();
            this.email = person.getEmail();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
