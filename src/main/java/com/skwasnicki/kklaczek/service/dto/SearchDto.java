package com.skwasnicki.kklaczek.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.skwasnicki.kklaczek.domain.types.City;

import java.time.LocalDateTime;

public class SearchDto {

    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private String fromPlace;
    private String toPlace;

    public SearchDto() {
    }

    public SearchDto(LocalDateTime fromDate, LocalDateTime toDate, String fromPlace, String toPlace) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromPlace = fromPlace;
        this.toPlace = toPlace;
    }

    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }
}
