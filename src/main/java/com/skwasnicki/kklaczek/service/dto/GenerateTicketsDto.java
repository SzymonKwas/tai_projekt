package com.skwasnicki.kklaczek.service.dto;

import java.time.LocalDateTime;

public class GenerateTicketsDto {
    private LocalDateTime startDate;
    private Integer howMany;

    public GenerateTicketsDto() {
    }

    public GenerateTicketsDto(LocalDateTime startDate, Integer howMany) {
        this.startDate = startDate;
        this.howMany = howMany;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public Integer getHowMany() {
        return howMany;
    }

    public void setHowMany(Integer howMany) {
        this.howMany = howMany;
    }
}
