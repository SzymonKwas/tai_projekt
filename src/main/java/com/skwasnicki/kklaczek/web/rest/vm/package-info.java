/**
 * View Models used by Spring MVC REST controllers.
 */
package com.skwasnicki.kklaczek.web.rest.vm;
