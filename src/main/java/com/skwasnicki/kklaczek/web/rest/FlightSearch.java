package com.skwasnicki.kklaczek.web.rest;

import com.skwasnicki.kklaczek.domain.Flight;
import com.skwasnicki.kklaczek.domain.Ticket;
import com.skwasnicki.kklaczek.domain.User;
import com.skwasnicki.kklaczek.repository.FlightRepository;
import com.skwasnicki.kklaczek.repository.TicketRepository;
import com.skwasnicki.kklaczek.repository.UserRepository;
import com.skwasnicki.kklaczek.security.AuthoritiesConstants;
import com.skwasnicki.kklaczek.service.SearchService;
import com.skwasnicki.kklaczek.service.UserService;
import com.skwasnicki.kklaczek.service.dto.SearchDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/flight")
public class FlightSearch {

    private SearchService searchService;
    private UserRepository userRepository;
    private TicketRepository ticketRepository;
    private FlightRepository flightRepository;

    public FlightSearch(SearchService searchService, UserRepository userRepository, TicketRepository ticketRepository) {
        this.searchService = searchService;
        this.userRepository = userRepository;
        this.ticketRepository = ticketRepository;
    }



    @PostMapping("/search")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.FULL_USER + "\")")
    public ResponseEntity<List<Flight>> searchFlightBetween(@RequestBody SearchDto searchDto){
        List<Flight> flights = searchService.getFlightBetween(searchDto);

        return new ResponseEntity<>(flights, HttpStatus.OK);
    }



}
