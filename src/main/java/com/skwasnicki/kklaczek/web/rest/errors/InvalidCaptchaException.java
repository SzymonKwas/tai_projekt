package com.skwasnicki.kklaczek.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class InvalidCaptchaException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public InvalidCaptchaException() {
        super(ErrorConstants.INVALID_PASSWORD_TYPE, "Invalid Captcha", Status.BAD_REQUEST);
    }
}
