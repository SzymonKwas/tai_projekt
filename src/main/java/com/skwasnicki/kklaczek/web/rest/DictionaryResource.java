package com.skwasnicki.kklaczek.web.rest;

import com.skwasnicki.kklaczek.mapper.DictionaryMapper;
import com.skwasnicki.kklaczek.repository.DictionaryCityRepository;
import com.skwasnicki.kklaczek.repository.DictionaryCountryRepository;
import com.skwasnicki.kklaczek.service.DictionaryService;
import com.skwasnicki.kklaczek.service.dto.DictionaryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/dictionary")
public class DictionaryResource {

    private final DictionaryService dictionaryService;
    private final DictionaryCityRepository dictionaryCityRepository;
    private final DictionaryCountryRepository dictionaryCountryRepository;

    private final DictionaryMapper dictionaryMapper;

    @Autowired
    public DictionaryResource(
        DictionaryService dictionaryService,
        DictionaryCityRepository dictionaryCityRepository,
        DictionaryCountryRepository dictionaryCountryRepository,
        DictionaryMapper dictionaryMapper) {
        this.dictionaryService = dictionaryService;
        this.dictionaryCityRepository = dictionaryCityRepository;
        this.dictionaryCountryRepository = dictionaryCountryRepository;
        this.dictionaryMapper = dictionaryMapper;
    }


//    @GetMapping("cities")
//    ResponseEntity<Map<String, Object>> getCities() {
//        try {
//            return new ResponseEntity<>(dictionaryService.getCitiesDictionary(), HttpStatus.OK);
//        } catch (IOException e) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//    @GetMapping("countries")
//    ResponseEntity<Map<String, Object>> getCountries() {
//        try {
//            return new ResponseEntity<>(dictionaryService.getCountriesDictionary(), HttpStatus.OK);
//        } catch (IOException e) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @GetMapping("cities")
    ResponseEntity<Map<String, String>> getCities() {
        final Map<String, String> dictionary = dictionaryCityRepository.findAll()
            .stream()
            .filter(Objects::nonNull)
            .map((dictionaryMapper::dictionaryCityToDictionaryDTO))
            .collect(Collectors.toMap(DictionaryDTO::getCode, DictionaryDTO::getName));
        return new ResponseEntity<>(dictionary, HttpStatus.OK);

    }

    @GetMapping("countries")
    ResponseEntity<Map<String, String>> getCountries() {
        final Map<String, String> dictionary = dictionaryCountryRepository.findAll()
            .stream()
            .filter(Objects::nonNull)
            .map((dictionaryMapper::dictionaryConutryToDictionaryDTO))
            .collect(Collectors.toMap(DictionaryDTO::getCode, DictionaryDTO::getName));
        return new ResponseEntity<>(dictionary, HttpStatus.OK);
    }
}
