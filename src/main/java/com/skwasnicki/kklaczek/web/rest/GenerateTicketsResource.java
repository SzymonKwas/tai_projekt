package com.skwasnicki.kklaczek.web.rest;

import com.skwasnicki.kklaczek.domain.Flight;
import com.skwasnicki.kklaczek.domain.Ticket;
import com.skwasnicki.kklaczek.domain.types.City;
import com.skwasnicki.kklaczek.domain.types.Planes;
import com.skwasnicki.kklaczek.repository.FlightRepository;
import com.skwasnicki.kklaczek.repository.TicketRepository;
import com.skwasnicki.kklaczek.security.AuthoritiesConstants;
import com.skwasnicki.kklaczek.service.dto.GenerateTicketsDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/generate")
@PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
public class GenerateTicketsResource {

    private TicketRepository ticketRepository;
    private FlightRepository flightRepository;

    public GenerateTicketsResource(TicketRepository ticketRepository, FlightRepository flightRepository) {
        this.ticketRepository = ticketRepository;
        this.flightRepository = flightRepository;
    }

    @PostMapping("flights")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void generateFlights(@RequestBody GenerateTicketsDto generateTicketsDto){
        Random rnd = new Random();
       // List<Flight> flights = new ArrayList<>();
        for(int i = 0; i < generateTicketsDto.getHowMany() ; i++){
            Integer rndDayStart = rnd.nextInt(1) + 25;
            Integer rndHoursStart = rnd.nextInt(1) + 23;
            Integer rndMinutesStart = rnd.nextInt(1) + 59;
            LocalDateTime genFlightDate = generateTicketsDto.getStartDate().plusDays(rndDayStart).plusHours(rndHoursStart).plusMinutes(rndMinutesStart);
            BigDecimal price= BigDecimal.valueOf((long) (rnd.nextInt(50) + 150));

            Planes plane = randomPlane();
            Flight flight = new Flight(plane, randomCity(), randomCity(), genFlightDate, BigDecimal.TEN,plane.getNumberOfPlaces());
//            flight = flightRepository.save(flight);
            List<Ticket> ticketList = new ArrayList<>();
            for(int j = 0; j < plane.getNumberOfPlaces() ; j++){
                ticketList.add(new Ticket(price, null, flight, flight.getCountOfLeftTickets()));
            }

            ticketRepository.saveAll(ticketList);
        }
       // flightRepository.saveAll(flights);

    }


    @PostMapping("/tickets")
    public void generateTickets(@RequestBody GenerateTicketsDto generateTicketsDto){

        Random rnd = new Random();
        List<Ticket> tickets = new ArrayList<>();
        for(int i = 0; i < generateTicketsDto.getHowMany() ; i++){
            Integer rndDayStart = rnd.nextInt(1) + 25;
            Integer rndHoursStart = rnd.nextInt(1) + 23;
            Integer rndMinutesStart = rnd.nextInt(1) + 59;
            LocalDateTime genFlightDate = generateTicketsDto.getStartDate().plusDays(rndDayStart).plusHours(rndHoursStart).plusMinutes(rndMinutesStart);
            City rndCountryFrom = randomCity();
            City rndCountryTo = randomCity();
            BigDecimal price = BigDecimal.valueOf((long) (rnd.nextInt(50) + 150));
            Long rndNumberOfPlaces = (long) rnd.nextInt(50) + 150;
           // tickets.add(new Ticket(price,rndCountryFrom,rndCountryTo,genFlightDate));

        }
        ticketRepository.saveAll(tickets);

    }

  /*  @PostMapping
    public void insertTicketToDB(@RequestBody SearchDto searchDto){
        ticketRepository.save(new Ticket( null,searchDto.getFromCity(),searchDto.getToCity(),searchDto.getFromDate()));
    }*/

    private City randomCity(){
        return City.values()[new Random().nextInt(City.values().length)];
    }

    private Planes randomPlane(){ return Planes.values()[new Random().nextInt(Planes.values().length)];}
}
