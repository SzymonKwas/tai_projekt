package com.skwasnicki.kklaczek.web.rest;

import com.skwasnicki.kklaczek.domain.BookTicket;
import com.skwasnicki.kklaczek.domain.Ticket;
import com.skwasnicki.kklaczek.domain.User;
import com.skwasnicki.kklaczek.repository.BookTicketRepository;
import com.skwasnicki.kklaczek.repository.TicketRepository;
import com.skwasnicki.kklaczek.repository.UserRepository;
import com.skwasnicki.kklaczek.security.AuthoritiesConstants;
import com.skwasnicki.kklaczek.service.MailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/ticket")
@PreAuthorize("hasRole(\"" + AuthoritiesConstants.FULL_USER + "\")")
public class TicketResource {

    private UserRepository userRepository;
    private TicketRepository ticketRepository;
    private BookTicketRepository bookTicketRepository;
    private MailService mailService;

    public TicketResource(UserRepository userRepository, TicketRepository ticketRepository, BookTicketRepository bookTicketRepository, MailService mailService) {
        this.userRepository = userRepository;
        this.ticketRepository = ticketRepository;
        this.bookTicketRepository = bookTicketRepository;
        this.mailService = mailService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<Ticket> getTicket(@PathVariable Long id) {
        return new ResponseEntity<>(ticketRepository.findById(id).get(), HttpStatus.OK);
    }

    @PostMapping("/book")
    public void bookTicket(@RequestBody List<BookTicket> bookTickets, Principal principal) {
        if (bookTickets != null) {

            User user = userRepository.findOneByLogin(principal.getName()).orElseThrow(NullPointerException::new);

            for (BookTicket bookTicket : bookTickets) {
                bookTicketRepository.save(bookTicket);
                bookTicket.setUser(user);
                bookTicketRepository.save(bookTicket);
            }
            this.mailService.sendReservationEmail(user, bookTickets);
        }
    }
}
