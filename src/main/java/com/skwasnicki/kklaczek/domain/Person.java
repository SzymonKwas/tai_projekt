package com.skwasnicki.kklaczek.domain;

import com.skwasnicki.kklaczek.domain.common.DataObject;
import com.skwasnicki.kklaczek.domain.types.Sex;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "PERSON")
@SequenceGenerator(name="idgen", sequenceName = "seq_person_id")
public class Person extends DataObject {

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "address_id")
    private Address address;

    @Column(name = "SEX", length = 6)
    private Sex sex;

    @Column(length = 50, name = "LAST_NAME")
    private String lastName;

    @Column(length = 30, name = "FIRST_NAME")
    private String firstName;

    @Column(length = 30, name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "BIRTH_DATE")
    private LocalDate birthDate;

    @Column(name = "PESEL", length = 11)
    private String pesel;

    @Column(length = 30, name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(length = 50)
    private String email;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person{" +
            "address='" + address + '\'' +
            ", sex=" + sex +
            ", lastName='" + lastName + '\'' +
            ", firstName='" + firstName + '\'' +
            ", middleName='" + middleName + '\'' +
            ", birthDate=" + birthDate +
            ", pesel='" + pesel + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", email='" + email + '\'' +
            '}';
    }
}
