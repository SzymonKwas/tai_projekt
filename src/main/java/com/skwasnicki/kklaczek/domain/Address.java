package com.skwasnicki.kklaczek.domain;


import com.skwasnicki.kklaczek.domain.common.DataObject;
import com.skwasnicki.kklaczek.domain.types.AddressType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ADDRESS")
@SequenceGenerator(name="idgen", sequenceName = "seq_address_id")
public class Address extends DataObject {

    @Column(length = 2)
    private String country;

    @Column(length = 10, name = "POSTAL_CODE")
    private String postalCode;

    @Column(length = 50)
    private String city;

    @Column(length = 50)
    private String street;

    @Column(length = 10, name = "BUILDING_NUMBER")
    private String buildingNumber;

    @Column(length = 10, name = "FLAT_NUMBER")
    private String flatNumber;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }
}
