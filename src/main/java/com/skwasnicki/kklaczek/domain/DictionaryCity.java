package com.skwasnicki.kklaczek.domain;

import com.skwasnicki.kklaczek.domain.common.DataObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DictionaryCity")
@SequenceGenerator(name = "idgen", sequenceName = "seq_dictionary_city_id")
public class DictionaryCity extends DataObject {

    @Column(length = 5)
    String code;

    @Column(length = 50)
    String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
