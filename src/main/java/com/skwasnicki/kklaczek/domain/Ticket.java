package com.skwasnicki.kklaczek.domain;

import com.skwasnicki.kklaczek.domain.common.DataObject;
import com.skwasnicki.kklaczek.domain.types.City;
import com.skwasnicki.kklaczek.domain.types.Country;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "Ticket")
@SequenceGenerator(name="idgen", sequenceName = "seq_ticket_id")
public class Ticket extends DataObject {

    private BigDecimal price;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "flight_id")
    private Flight flight;

    private Integer countOfLeftTickets;

    public Ticket(BigDecimal price, User user, Flight flight) {
        this.price = price;
        this.user = user;
        this.flight = flight;
    }

    public Ticket(BigDecimal price, User user, Flight flight, Integer countOfLeftTickets) {
        this.price = price;
        this.user = user;
        this.flight = flight;
        this.countOfLeftTickets = countOfLeftTickets;
    }


    public Ticket() {
    }

    public Integer getCountOfLeftTickets() {
        return countOfLeftTickets;
    }

    public void setCountOfLeftTickets(Integer countOfLeftTickets) {
        this.countOfLeftTickets = countOfLeftTickets;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }
}
