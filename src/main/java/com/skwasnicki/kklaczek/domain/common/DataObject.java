package com.skwasnicki.kklaczek.domain.common;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public abstract class DataObject implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idgen")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
