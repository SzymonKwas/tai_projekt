package com.skwasnicki.kklaczek.domain.types;

public enum Sex {
    MALE("MALE"), FEMALE("FEMALEF");

    public final String value;

    Sex(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
