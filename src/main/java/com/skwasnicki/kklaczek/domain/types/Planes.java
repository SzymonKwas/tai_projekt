package com.skwasnicki.kklaczek.domain.types;

public enum Planes {
    Boeing_787(250),
    Airbus_A320(300),
    Cessna_404(150),
    Embraer_195(100);


    private Integer numberOfPlaces;

    Planes(Integer numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public Integer getNumberOfPlaces() {
        return numberOfPlaces;
    }

}
