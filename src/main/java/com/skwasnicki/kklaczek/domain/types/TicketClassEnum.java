package com.skwasnicki.kklaczek.domain.types;

public enum TicketClassEnum {
    ECONOMY("Economy"),
    BUSINESS("Business");

    private String name;

    TicketClassEnum(String name) {
        this.name = name;
    }
}
