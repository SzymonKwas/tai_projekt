package com.skwasnicki.kklaczek.domain.types;

public enum TicketTypeEnum {
    SEAT_INFANT("Seat infant"),
    CHILD ("Child"),
    YOUTH("Youth"),
    ADULT("Adult"),
    SENIOR("Senior");

    private String name;

    TicketTypeEnum(String name) {
        this.name = name;
    }
}
