package com.skwasnicki.kklaczek.domain.types;

public enum Country {
    PL("Poland"),
    DE("Germany"),
    BR("Brazil"),
    CN("China"),
    FI("Finland"),
    FR("France"),
    CA("Canada"),
    SE("Sweden"),
    IT("Italy"),
    GB("United Kingdom");

    private String name;

    Country(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
