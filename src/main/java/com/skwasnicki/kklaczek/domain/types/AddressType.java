package com.skwasnicki.kklaczek.domain.types;

public enum AddressType {
    CORRESPONDENCE("CR"),
    LIVING("LI");

    public final String value;

    AddressType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
