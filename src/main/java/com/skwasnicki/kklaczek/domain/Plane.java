package com.skwasnicki.kklaczek.domain;

import com.skwasnicki.kklaczek.domain.common.DataObject;

import javax.persistence.*;

@Entity
@Table(name = "PLANE")
@SequenceGenerator(name="idgen", sequenceName = "seq_plane_id")
public class Plane extends DataObject {

    private Long numberOfPlaces;

    public Plane(Long numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public Plane() {
    }

    public Long getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(Long numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }
}
