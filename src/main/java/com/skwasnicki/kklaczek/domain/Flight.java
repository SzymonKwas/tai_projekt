package com.skwasnicki.kklaczek.domain;

import com.skwasnicki.kklaczek.domain.common.DataObject;
import com.skwasnicki.kklaczek.domain.types.City;
import com.skwasnicki.kklaczek.domain.types.Planes;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "Flight")
@SequenceGenerator(name = "idgen", sequenceName = "seq_flight_id")
public class Flight extends DataObject {
    private Planes plane;

    private City fromCity;

    private City toCity;

    @DateTimeFormat
    private LocalDateTime date;

    private BigDecimal price;

    private Integer countOfLeftTickets;

    public Flight() {

    }

    public Flight(Planes plane, City fromCity, City toCity, LocalDateTime date) {
        this.plane = plane;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.date = date;
    }

    public Flight(Planes plane, City fromCity, City toCity, LocalDateTime date, Integer countOfLeftTickets) {
        this.plane = plane;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.date = date;
        this.countOfLeftTickets = countOfLeftTickets;
    }

    public Flight(Planes plane, City fromCity, City toCity, LocalDateTime date, BigDecimal price, Integer countOfLeftTickets) {
        this.plane = plane;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.date = date;
        this.price = price;
        this.countOfLeftTickets = countOfLeftTickets;
    }

    public Planes getPlane() {
        return plane;
    }

    public void setPlane(Planes plane) {
        this.plane = plane;
    }

    public City getFromCity() {
        return fromCity;
    }

    public void setFromCity(City fromCity) {
        this.fromCity = fromCity;
    }

    public City getToCity() {
        return toCity;
    }

    public void setToCity(City toCity) {
        this.toCity = toCity;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCountOfLeftTickets() {
        return countOfLeftTickets;
    }

    public void setCountOfLeftTickets(Integer countOfLeftTickets) {
        this.countOfLeftTickets = countOfLeftTickets;
    }
}

