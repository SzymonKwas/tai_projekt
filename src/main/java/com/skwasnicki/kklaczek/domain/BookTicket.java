package com.skwasnicki.kklaczek.domain;

import com.skwasnicki.kklaczek.domain.common.DataObject;
import com.skwasnicki.kklaczek.domain.types.TicketClassEnum;
import com.skwasnicki.kklaczek.domain.types.TicketTypeEnum;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@SequenceGenerator(name="idgen", sequenceName = "seq_book_ticket_id")
public class BookTicket extends DataObject {

    @ManyToOne()
    @JoinColumn(name = "flight_id")
    private Flight flight;

    private String ticketClass;

    private String ticketType;

    private BigDecimal price;

    private String firstName;

    private String lastName;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    private User user;


    public BookTicket() {
    }

    public BookTicket(Flight flight, String ticketClass, String ticketType, BigDecimal price, String firstName, String lastName, User user) {
        this.flight = flight;
        this.ticketClass = ticketClass;
        this.ticketType = ticketType;
        this.price = price;
        this.firstName = firstName;
        this.lastName = lastName;
        this.user = user;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public String getTicketClass() {
        return ticketClass;
    }

    public void setTicketClass(String ticketClass) {
        this.ticketClass = ticketClass;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
