package com.skwasnicki.kklaczek.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * reCAPTCHA configuration properties.
 */

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class RecaptchaProperties {

    public RecaptchaProperties() {

    }

    private String secretKey;

    private String validationKey;


    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getValidationKey() {
        return validationKey;
    }

    public void setValidationKey(String validationKey) {
        this.validationKey = validationKey;
    }
}
