import * as UserActions from '../actions/user.actions';
import { UserState } from '../states/user.state';

const initialState: UserState = {
    login: null,
    person: {
        id: null,
        address: {
            id: null,
            country: null,
            postalCode: null,
            city: null,
            street: null,
            buildingNumber: null,
            flatNumber: null
        },
        sex: null,
        lastName: null,
        firstName: null,
        middleName: null,
        birthDate: null,
        pesel: null,
        phoneNumber: null,
        email: null
    },
    langKey: null
};

export function UserReducer(state = initialState, action: UserActions.Actions) {
    switch (action.type) {
        case UserActions.ADD_USER:
            return action.user;

        default:
            return state;
    }
}
