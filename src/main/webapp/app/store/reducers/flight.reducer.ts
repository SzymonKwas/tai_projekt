import { FlightsState } from '../states/flight.state';
import * as FlightActions from '../actions/flight.actions';

const initialState: FlightsState = {
    flights: []
};

export function FlightReducer(state = initialState, action: FlightActions.Actions) {
    switch (action.type) {
        case FlightActions.SEARCH_FLIGTHS_SUCCESS:
            return action;

        default:
            return state;
    }
}
