import * as DictionaryActions from '../actions/dictionary.actions';
import { DictionaryState } from '../states/dictionary.state';

const initialState: DictionaryState = {
    cities: null,
    countries: null
};

export function DictionaryReducer(state = initialState, action: DictionaryActions.Actions) {
    switch (action.type) {
        case DictionaryActions.GET_CITIES_SUCCESS:
            return { ...state, cities: action };
        case DictionaryActions.GET_COUNTRIES_SUCCESS:
            return { ...state, countries: action };
        default:
            return state;
    }
}
