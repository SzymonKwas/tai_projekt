import { DictionaryCity, DictionaryCountry, DictionaryElement } from '../../shared/commons/dictionary.data';

export interface DictionaryState {
    cities: DictionaryCityState;
    countries: DictionaryCountryState;
}

export interface DictionaryCityState extends DictionaryCity {}
export interface DictionaryCountryState extends DictionaryCountry {}

export interface City extends DictionaryElement {}
export interface Country extends DictionaryElement {}
