import { Address } from '../../shared/models/address.model';

export interface AddressState extends Address {}
