import { Flight } from '../../shared/models/flight.model';

export interface FlightsState {
    flights: FlightState[];
}

export interface FlightState extends Flight {}
