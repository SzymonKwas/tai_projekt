import { PersonState } from './person.state';

export interface UserState {
    login: string;
    person: PersonState;
    langKey: string;
}
