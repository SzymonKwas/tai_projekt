import { Action } from '@ngrx/store';
import { UserState } from '../states/user.state';

export const ADD_USER = 'ADD_USER';

export class AddUserAction implements Action {
    readonly type = ADD_USER;
    constructor(public user: UserState) {}
}

export type Actions = AddUserAction;
