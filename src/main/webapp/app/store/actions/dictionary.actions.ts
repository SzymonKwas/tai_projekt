import { Action } from '@ngrx/store';
import { DictionaryCityState, DictionaryCountryState } from '../states/dictionary.state';

export const GET_CITIES = 'GET_CITIES';
export const GET_CITIES_SUCCESS = 'GET_CITIES_SUCCESS';
export const GET_COUNTRIES = 'GET_COUNTRIES';
export const GET_COUNTRIES_SUCCESS = 'GET_COUNTRIES_SUCCESS';

export class GetCitiesAction implements Action {
    readonly type = GET_CITIES;

    constructor() {}
}

export class GetCitiesActionSuccess implements Action {
    readonly type = GET_CITIES_SUCCESS;

    constructor(public cities: DictionaryCityState) {}
}

export class GetCountriesAction implements Action {
    readonly type = GET_COUNTRIES;

    constructor() {}
}

export class GetCountriesActionSuccess implements Action {
    readonly type = GET_COUNTRIES_SUCCESS;

    constructor(public countries: DictionaryCountryState) {}
}

export type Actions = GetCitiesAction | GetCitiesActionSuccess | GetCountriesAction | GetCountriesActionSuccess;
