import { Action } from '@ngrx/store';

export const NOTHING = 'NOTHING';

export class NothingAction implements Action {
    readonly type = NOTHING;
    constructor() {}
}

export type Actions = NothingAction;
