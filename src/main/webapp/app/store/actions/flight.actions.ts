import { Action } from '@ngrx/store';
import { Search } from '../../shared/models/search.mode';
import { Flight } from '../../shared/models/flight.model';

export const SEARCH_FLIGHTS = 'SEARCH_FLIGHTS';
export const SEARCH_FLIGTHS_SUCCESS = 'SEARCH_FLIGTHS_SUCCESS';

export class SearchFlightsAction implements Action {
    readonly type = SEARCH_FLIGHTS;

    constructor(public search: Search) {}
}

export class SearchFlightsActionSuccess implements Action {
    readonly type = SEARCH_FLIGTHS_SUCCESS;

    constructor(public flights: Flight[]) {}
}

export type Actions = SearchFlightsAction | SearchFlightsActionSuccess;
