import { ActionReducerMap } from '@ngrx/store';
import { AppState } from './app.state';

import { UserReducer } from './reducers/user.reducer';
import { DictionaryReducer } from './reducers/dictionary.reducers';
import { FlightReducer } from './reducers/flight.reducer';

export const AppReducer: ActionReducerMap<AppState> = {
    user: UserReducer,
    searchedFlights: FlightReducer,
    dictionary: DictionaryReducer
} as any;
