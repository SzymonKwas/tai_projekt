import { UserState } from './states/user.state';
import { DictionaryState } from './states/dictionary.state';
import { FlightsState } from './states/flight.state';

export interface AppState {
    user: UserState;
    dictionary: DictionaryState;
    searchedFlights: FlightsState;
}
