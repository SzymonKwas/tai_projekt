import { Type } from '@angular/core';
import { UserEffects } from './effects/user.effect';
import { DictionaryEffects } from './effects/dictionary.effect';
import { FlightsEffects } from './effects/flights.effect';

export const AppEffects: Type<any>[] = [UserEffects, DictionaryEffects, FlightsEffects];
