import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as flightActions from '../actions/flight.actions';
import { map } from 'rxjs/operators';
import { catchError, switchMap } from 'rxjs/internal/operators';
import { SearchService } from '../../shared/services/search.service';
import { Flight } from '../../shared/models/flight.model';
import { of } from 'rxjs/index';

@Injectable()
export class FlightsEffects {
    @Effect()
    searchFlights$ = this.actions$.pipe(
        ofType(flightActions.SEARCH_FLIGHTS),
        switchMap((action: flightActions.SearchFlightsAction) =>
            this.searchService.searchFlights(action.search).pipe(
                map((data: Flight[]) => {
                    console.log(data);
                    return new flightActions.SearchFlightsActionSuccess(data);
                }),
                catchError(() => of(new flightActions.SearchFlightsActionSuccess([])))
            )
        )
    );

    constructor(private actions$: Actions, private searchService: SearchService) {}
}
