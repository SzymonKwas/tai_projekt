import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as UserActions from '../actions/user.actions';
import { map, withLatestFrom } from 'rxjs/operators';
import { AppState } from '../app.state';
import { Store } from '@ngrx/store';

@Injectable()
export class UserEffects {
    @Effect({ dispatch: false })
    addUser$ = this.actions$.pipe(
        ofType(UserActions.ADD_USER),
        withLatestFrom(this.store$.select(state => state.user)),
        map(([_action, state]) => {
            console.log(_action, state);
        })
    );

    constructor(private actions$: Actions, private store$: Store<AppState>) {}
}
