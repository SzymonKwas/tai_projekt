import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import * as dictionaryActions from '../actions/dictionary.actions';
import { map } from 'rxjs/operators';
import { AppState } from '../app.state';
import { Store } from '@ngrx/store';
import { DictionaryService } from '../../shared/services/dictionary.service';
import { catchError, switchMap } from 'rxjs/internal/operators';
import { of } from 'rxjs';

@Injectable()
export class DictionaryEffects {
    @Effect()
    getCities$ = this.actions$.ofType(dictionaryActions.GET_CITIES).pipe(
        switchMap(() =>
            this.dictionaryService.getCitiesDictionary().pipe(
                map((data: any) => {
                    console.log(data);
                    return new dictionaryActions.GetCitiesActionSuccess(data);
                }),
                catchError(() => of(new dictionaryActions.GetCitiesActionSuccess(null)))
            )
        )
    );

    @Effect()
    getCountries$ = this.actions$.ofType(dictionaryActions.GET_COUNTRIES).pipe(
        switchMap(() =>
            this.dictionaryService.getCountriesDictionary().pipe(
                map((data: any) => {
                    console.log(data);
                    return new dictionaryActions.GetCountriesActionSuccess(data);
                }),
                catchError(() => of(new dictionaryActions.GetCountriesActionSuccess(null)))
            )
        )
    );

    constructor(private actions$: Actions, private store$: Store<AppState>, private dictionaryService: DictionaryService) {}
}
