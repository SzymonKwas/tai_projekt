export enum TicketTypeEnum {
    SEAT_INFANT = 'Seat infant',
    CHILD = 'Child',
    YOUTH = 'Youth',
    ADULT = 'Adult',
    SENIOR = 'Senior'
}
