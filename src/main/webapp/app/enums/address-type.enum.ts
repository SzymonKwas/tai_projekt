export enum AddressTypeEnum {
    CORRESPONDENCE = 'CR',
    LIVING = 'LI'
}
