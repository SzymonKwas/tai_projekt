import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SEARCH_ROUTE } from './search.route';
import { SearchComponent } from './search.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { TaiProjektSharedModule } from '../shared/shared.module';
import { MatAutocompleteModule, MatIconModule, MatMenuModule } from '@angular/material';
import { DictValuesPipe } from '../shared/pipes/dict-values.pipe';
import { SearchListComponent } from './search-result/search-list.component';
import { SearchItemComponent } from './search-result/search-result-item/search-item.component';

@NgModule({
    imports: [
        TaiProjektSharedModule,
        RouterModule.forChild([SEARCH_ROUTE]),
        CommonModule,
        FormsModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'always' }),
        MatInputModule,
        MatIconModule,
        MatAutocompleteModule,
        MatMenuModule,
        MatToolbarModule,
        MatTooltipModule,
        MatButtonModule,
        MatDatepickerModule
    ],
    providers: [DictValuesPipe],
    declarations: [SearchComponent, SearchInputComponent, SearchListComponent, SearchItemComponent]
})
export class SearchModule {}
