import { Component, Input } from '@angular/core';
import { Flight } from '../../shared/models/flight.model';
import { DictionaryCityState } from '../../store/states/dictionary.state';

@Component({
    selector: 'jhi-search-list-result',
    templateUrl: './search-list.template.html'
})
export class SearchListComponent {
    @Input()
    flights: Flight[];

    @Input()
    dictionaryCity: DictionaryCityState;
}
