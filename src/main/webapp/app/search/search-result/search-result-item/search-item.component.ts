import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Flight } from '../../../shared/models/flight.model';
import { Router } from '@angular/router';
import { DictionaryCityState } from '../../../store/states/dictionary.state';

@Component({
    selector: 'jhi-search-item',
    templateUrl: './search-item.template.html',
    styleUrls: ['./search-item.scss']
})
export class SearchItemComponent implements OnInit {
    @Input()
    flight: Flight;
    @Input()
    dictionaryCity: DictionaryCityState;

    constructor(private dialog: MatDialog, private router: Router) {}

    ngOnInit() {}

    openTickets(flightId: number) {
        this.router.navigate(['/ticket', flightId]);
    }

    toCityName(cityCode) {
        return this.dictionaryCity.cities[cityCode];
    }
}
