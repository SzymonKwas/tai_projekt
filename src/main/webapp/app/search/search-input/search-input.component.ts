import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { City, DictionaryCityState } from '../../store/states/dictionary.state';
import { Observable } from 'rxjs/index';
import { map, startWith } from 'rxjs/internal/operators';
import { DictValuesPipe } from '../../shared/pipes/dict-values.pipe';
import { MatDatepicker } from '@angular/material';

@Component({
    selector: 'jhi-search-input',
    templateUrl: './search-input.component.html',
    styleUrls: ['./search-input.scss']
})
export class SearchInputComponent implements OnInit {
    @Input()
    group: FormGroup;
    @Input()
    dictionaryCity: DictionaryCityState;
    @Input()
    fromPlaceControl: FormControl;
    @Input()
    toPlaceControl: FormControl;
    @Input()
    fromDateControl: FormControl;
    @Input()
    toDateControl: FormControl;

    @Output()
    inputChanged = new EventEmitter<string>();
    @Output()
    searchBtnUsed = new EventEmitter<boolean>();

    minDate = new Date();
    cities: City[] = null;
    fromFiltredCities: Observable<City[]>;
    toFiltredCities: Observable<City[]>;

    constructor(private dictValuesPipe: DictValuesPipe) {}

    mapDictionary() {
        this.cities = this.dictValuesPipe.transform(this.dictionaryCity.cities);
    }

    ngOnInit() {
        this.fromFiltredCities = this.fromPlaceControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );

        this.toFiltredCities = this.toPlaceControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
        this.mapDictionary();
    }

    private _filter(value: any): City[] {
        const filterValue = !!value.name ? value.name.toLowerCase() : value.toLowerCase();

        return !!this.cities ? this.cities.filter(option => option.name.toLowerCase().includes(filterValue)) : null;
    }

    onDateInput(event: KeyboardEvent, matDatepicker: MatDatepicker<any>) {
        switch (event.key) {
            case 'Escape':
            case 'Enter':
                matDatepicker.close();
                break;
            case 'Delete':
            case 'Backspace':
                matDatepicker.select(null);
                matDatepicker.close();
                break;
            case 'Tab':
                break;
            default:
                event.preventDefault();
        }
    }

    displayCityFn(city: City | string): string {
        if (typeof city === 'string') {
            return !!city ? city : '';
        } else {
            return city.name;
        }
    }

    btnClicked() {
        this.searchBtnUsed.emit(true);
    }
}
