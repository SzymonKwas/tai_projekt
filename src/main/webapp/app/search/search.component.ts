import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { AppState } from '../store/app.state';
import { GetCitiesAction } from '../store/actions/dictionary.actions';
import { distinctUntilChanged } from 'rxjs/internal/operators';
import { DictionaryCityState } from '../store/states/dictionary.state';
import { Observable } from 'rxjs/index';
import { SearchFlightsAction } from '../store/actions/flight.actions';
import { DictionaryElement } from '../shared/commons/dictionary.data';
import * as moment from 'moment';
import { Flight } from '../shared/models/flight.model';

@Component({
    selector: 'jhi-search',
    templateUrl: './search.component.html',
    styles: []
})
export class SearchComponent implements OnInit {
    searchFormGroup: FormGroup;
    fromPlaceControl: AbstractControl;
    toPlaceControl: FormControl;
    fromDateControl: FormControl;
    toDateControl: FormControl;

    flights$: Observable<Flight[]>;

    citiesDictionary$: Observable<DictionaryCityState>;

    constructor(private formBuilder: FormBuilder, private store$: Store<AppState>) {}

    ngOnInit() {
        this.fromPlaceControl = this.formBuilder.control('');
        this.toPlaceControl = this.formBuilder.control('');
        this.fromDateControl = this.formBuilder.control(moment(new Date()));
        this.toDateControl = this.formBuilder.control('');

        this.searchFormGroup = this.formBuilder.group({
            fromPlaceControl: this.fromPlaceControl,
            toPlaceControl: this.toPlaceControl,
            fromDateControl: this.fromDateControl,
            toDateControl: this.toDateControl
        });

        this.store$.dispatch(new GetCitiesAction());
        this.citiesDictionary$ = this.store$.pipe(select(state => state.dictionary.cities, distinctUntilChanged()));
    }

    search(): void {
        this.store$.dispatch(
            new SearchFlightsAction({
                fromDate: this.parseMomentToString(this.searchFormGroup.value.fromDateControl),
                toDate: this.parseMomentToString(this.searchFormGroup.value.toDateControl),
                fromPlace: this.parseCityToString(this.searchFormGroup.value.fromPlaceControl),
                toPlace: this.parseCityToString(this.searchFormGroup.value.toPlaceControl)
            })
        );
        this.flights$ = this.store$.pipe(select(state => state.searchedFlights.flights));
    }

    private parseMomentToString(momentDate: moment.Moment): string {
        return !!momentDate ? momentDate.toISOString() : '';
    }

    private parseCityToString(city: DictionaryElement): string {
        return !!city && city.code ? city.code : '';
    }
}
