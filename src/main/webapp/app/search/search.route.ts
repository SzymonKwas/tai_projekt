import { Route } from '@angular/router';
import { SearchComponent } from './search.component';
import { UserRouteAccessService } from '../core/auth/user-route-access-service';

export const SEARCH_ROUTE: Route = {
    path: 'search',
    component: SearchComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Fly searcher'
    },
    canActivate: [UserRouteAccessService]
};
