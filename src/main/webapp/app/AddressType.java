public enum AddressType {
    CORRESPONDENCE("CR"),
    LIVING("LI");
