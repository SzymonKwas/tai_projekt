import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs/index';
import { Ticket, BookTicket } from '../shared/models/ticket.model';
import { delay } from 'rxjs/internal/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TicketService {
    constructor(private http: HttpClient) {}

    getTicket(id: number): Observable<Ticket> {
        if (environment.mockTicket) {
            return this.mockTicket();
        } else {
            return this.http.get<Ticket>(`/api/ticket/${id}`);
        }
    }

    bookTickets(bookedTickets: BookTicket[]): Observable<any> {
        if (environment.mockTicketBook) {
            return of(null);
        } else {
            return this.http.post<BookTicket[]>(`/api/ticket/book`, bookedTickets);
        }
    }

    private mockTicket(): Observable<Ticket> {
        return of({
            id: 1,
            flight: {
                id: 1,
                fromCity: 'KRK',
                toCity: 'WAW',
                date: '2019-01-12:00:00',
                price: 120
            },
            time: 95,
            countOfLeftTickets: 7
        }).pipe(delay(1000));
    }
}
