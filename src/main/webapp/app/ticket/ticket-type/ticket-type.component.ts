import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { TicketTypeEnum } from '../../enums/ticket-type.enum';
import { TicketClassEnum } from '../../enums/ticket-class.enum';
import { Subscription } from 'rxjs';

@Component({
    selector: 'jhi-ticket-type',
    templateUrl: './ticket-type.component.html',
    styleUrls: ['./ticket-type.scss']
})
export class TicketTypeComponent implements OnInit, OnDestroy {
    @Input()
    formGroup: FormGroup;
    @Input()
    no: number;
    @Input()
    basicPrice: number;

    readonly TicketTypeEnum = TicketTypeEnum;
    readonly TicketClassEnum = TicketClassEnum;

    ticketClasses = [{ name: 'Economy', id: TicketClassEnum.ECONOMY }, { name: 'Business', id: TicketClassEnum.BUSINESS }];

    ticketTypes = [
        { name: 'Senior (65 & above)', id: TicketTypeEnum.SENIOR },
        { name: 'Adult (18-64 yrs)', id: TicketTypeEnum.ADULT },
        { name: 'Youth (7-17 yrs)', id: TicketTypeEnum.YOUTH },
        { name: 'Child (2-6 yrs)', id: TicketTypeEnum.CHILD },
        { name: 'Seat infant (under 2)', id: TicketTypeEnum.SEAT_INFANT }
    ];

    ticketClassSubscription: Subscription;
    ticketTypeSubscription: Subscription;

    ticketType: AbstractControl;
    ticketClass: AbstractControl;
    price: AbstractControl;

    formErrors: any = {
        ticketType: '',
        ticketClass: ''
    };

    constructor() {}

    ngOnInit() {
        this.ticketType = this.formGroup.controls['ticketType'];
        this.ticketClass = this.formGroup.controls['ticketClass'];
        this.price = this.formGroup.controls['price'];

        this.ticketTypeSubscription = this.ticketType.valueChanges.subscribe(() => this.calculatePrice());
        this.ticketClassSubscription = this.ticketClass.valueChanges.subscribe(() => this.calculatePrice());
    }

    setTicketClass(id) {
        this.ticketClass.setValue(id);
    }

    calculatePrice() {
        let priceValue = this.basicPrice;

        switch (this.ticketClass.value) {
            case TicketClassEnum.BUSINESS:
                priceValue = this.basicPrice * 2;
                break;
            default:
                break;
        }

        switch (this.ticketType.value) {
            case TicketTypeEnum.SENIOR:
            case TicketTypeEnum.CHILD:
                priceValue = priceValue - this.basicPrice * 0.4;
                break;
            case TicketTypeEnum.YOUTH:
                priceValue = priceValue - this.basicPrice * 0.25;
                break;
            case TicketTypeEnum.SEAT_INFANT:
                priceValue = priceValue - this.basicPrice * 0.9;
                break;
            case TicketTypeEnum.ADULT:
            default:
                break;
        }
        this.price.setValue(priceValue);
    }

    ngOnDestroy(): void {
        if (this.ticketTypeSubscription) {
            this.ticketTypeSubscription.unsubscribe();
        }
        if (this.ticketClassSubscription) {
            this.ticketClassSubscription.unsubscribe();
        }
    }
}
