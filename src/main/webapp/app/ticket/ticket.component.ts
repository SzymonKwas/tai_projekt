import { Component, OnInit, ViewChild } from '@angular/core';
import { Ticket, BookTicket } from '../shared/models/ticket.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TicketTypeEnum } from '../enums/ticket-type.enum';
import { TicketClassEnum } from '../enums/ticket-class.enum';
import { MatSelectChange, MatStepper, MatDialog } from '@angular/material';
import { TicketService } from './ticket.service';
import { map, catchError, take } from 'rxjs/internal/operators';
import { GenericConfirmationComponent } from '../shared/dialogs/generic-confirmation/generic-confirmation.component';
import { of, pipe } from 'rxjs';

@Component({
    selector: 'jhi-ticket',
    templateUrl: './ticket.component.html',
    styleUrls: ['./ticket.scss']
})
export class TicketComponent implements OnInit {
    ticket: Ticket;
    nums: number[] = [];

    bookedTickets: BookTicket[] = [];

    @ViewChild('stepper')
    stepper: MatStepper;

    numberOfTickets = 0;
    selectedIndex = 0;
    enableSummary = false;

    summaryStepControl: FormGroup;

    formGroupPersonArray: FormGroup[];
    formGroupTypeArray: FormGroup[];

    constructor(
        private route: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private ticketService: TicketService,
        private router: Router,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        this.summaryStepControl = this._formBuilder.group({});
        this.ticket = this.route.snapshot.data['ticket'];

        for (let i = 0; i < this.ticket.countOfLeftTickets && i < 10; i++) {
            this.nums.push(i + 1);
        }
    }

    numberOfTicketsSelected(numberOfTickets: MatSelectChange) {
        this.formGroupPersonArray = [];
        this.formGroupTypeArray = [];
        this.bookedTickets = [];
        this.enableSummary = false;

        for (let i = 0; i < numberOfTickets.value; i++) {
            this.formGroupPersonArray.push(
                this._formBuilder.group({
                    firstName: [
                        {
                            value: '',
                            disabled: false
                        },
                        [Validators.required, Validators.maxLength(100)]
                    ],
                    lastName: [
                        {
                            value: '',
                            disabled: false
                        },
                        [Validators.required, Validators.maxLength(100)]
                    ]
                })
            );

            this.formGroupTypeArray.push(
                this._formBuilder.group({
                    ticketType: [
                        {
                            value: TicketTypeEnum.ADULT,
                            disabled: false
                        }
                    ],
                    ticketClass: [
                        {
                            value: TicketClassEnum.ECONOMY,
                            disabled: false
                        }
                    ],
                    price: [
                        {
                            value: this.ticket.flight.price,
                            disabled: true
                        }
                    ]
                })
            );
        }
        setTimeout(() => {
            this.stepper.selectedIndex = 0;
            this.selectedIndex = 0;
        }, 10);
    }

    checkValidation() {
        let valid = true;
        this.formGroupPersonArray.forEach(formPerson => {
            if (!formPerson.valid) {
                valid = false;
            }
        });

        this.enableSummary = valid;
    }

    assembleTickets() {
        this.bookedTickets = [];
        for (let i = 0; i < this.numberOfTickets; i++) {
            const bookTicket: BookTicket = {
                ...this.formGroupPersonArray[i].value,
                ...this.formGroupTypeArray[i].value,
                price: this.formGroupTypeArray[i].controls['price'].value,
                flight: this.ticket.flight
            };
            this.bookedTickets.push(bookTicket);
        }
    }

    selectionChange(event) {
        if (event.selectedIndex === 2) {
            this.assembleTickets();
        }
    }

    calculateSummaryPrice(): number {
        let summaryPrice = 0;
        this.bookedTickets.forEach(ticket => {
            summaryPrice += ticket.price;
        });
        return summaryPrice;
    }

    openDialog(header: string, content: string, successButton: string, redirect: boolean): void {
        const dialogRef = this.dialog.open(GenericConfirmationComponent, {
            width: '250px',
            data: {
                header,
                content,
                successButton
            }
        });

        if (redirect) {
            dialogRef
                .afterClosed()
                .pipe(take(1))
                .subscribe(_ => {
                    this.router.navigate(['/search']);
                });
        }
    }

    bookTickets() {
        this.ticketService.bookTickets(this.bookedTickets).subscribe(response => {
            return !response
                ? this.openDialog('Success', 'Tickets are booked!', 'Back', true)
                : this.openDialog('Failure', 'Error during booking', 'Back', false);
        });
    }
}
