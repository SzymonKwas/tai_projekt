import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { TicketService } from './ticket.service';
import { Ticket } from '../shared/models/ticket.model';
@Injectable()
export class TicketResolver implements Resolve<Ticket> {
    constructor(private ticketService: TicketService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.ticketService.getTicket(route.params['id']);
    }
}
