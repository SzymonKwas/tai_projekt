import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TICKET_ROUTE } from './ticket.route';
import { TicketComponent } from './ticket.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { TaiProjektSharedModule } from '../shared/shared.module';
import {
    MatAutocompleteModule,
    MatDialogModule,
    MatDividerModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatStepperModule
} from '@angular/material';
import { DictValuesPipe } from '../shared/pipes/dict-values.pipe';
import { TicketResolver } from './ticket.resolver';
import { TicketTypeComponent } from './ticket-type/ticket-type.component';
import { TicketPersonComponent } from './ticket-person/ticket-person.component';
import { GenericConfirmationModule } from '../shared/dialogs/generic-confirmation/generic-confirmation.module';

@NgModule({
    imports: [
        TaiProjektSharedModule,
        RouterModule.forChild(TICKET_ROUTE),
        CommonModule,
        FormsModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'always' }),
        MatInputModule,
        MatIconModule,
        MatAutocompleteModule,
        MatMenuModule,
        MatToolbarModule,
        MatTooltipModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatSelectModule,
        MatStepperModule,
        GenericConfirmationModule
    ],
    providers: [TicketResolver, DictValuesPipe],
    declarations: [TicketComponent, TicketPersonComponent, TicketTypeComponent]
})
export class TicketModule {}
