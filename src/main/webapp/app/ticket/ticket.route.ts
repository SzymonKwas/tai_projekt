import { Routes } from '@angular/router';
import { TicketComponent } from './ticket.component';
import { TicketResolver } from './ticket.resolver';
import { UserRouteAccessService } from '../core/auth/user-route-access-service';

export const TICKET_ROUTE: Routes = [
    {
        path: 'ticket/:id',
        component: TicketComponent,
        resolve: {
            ticket: TicketResolver
        },
        data: {
            authorities: ['ROLE_FULL_USER'],
            pageTitle: 'Ticket booking'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ticket/**',
        redirectTo: '/home',
        pathMatch: 'full',
        canActivate: [UserRouteAccessService]
    }
];
