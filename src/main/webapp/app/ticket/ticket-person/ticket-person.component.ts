import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { FormErrorService } from '../../shared/services/form-error.service';

@Component({
    selector: 'jhi-ticket-person',
    templateUrl: './ticket-person.component.html',
    styleUrls: ['./ticket-person.scss']
})
export class TicketPersonComponent implements OnInit {
    @Input()
    formGroup: FormGroup;
    @Input()
    no: number;

    @Output()
    personChanged = new EventEmitter<boolean>();

    formErrors: any = {
        firstName: '',
        lastName: ''
    };

    firstName: AbstractControl;
    lastName: AbstractControl;

    constructor(private formErrorService: FormErrorService) {}

    ngOnInit() {
        this.firstName = this.formGroup.controls['firstName'];
        this.lastName = this.formGroup.controls['lastName'];

        this.formErrorService.touchAllControls(this.formGroup);
        this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false);

        this.formGroup.valueChanges.subscribe(() => {
            this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false);
            this.personChanged.emit(true);
        });
    }
}
