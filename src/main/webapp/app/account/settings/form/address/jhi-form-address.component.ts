import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { AddressState } from '../../../../store/states/address.state';
import { distinctUntilChanged, map, pairwise, startWith } from 'rxjs/internal/operators';
import { FormErrorService } from '../../../../shared/services/form-error.service';
import { Country, DictionaryCountryState } from '../../../../store/states/dictionary.state';
import { JhiValidators } from '../../../../shared/services/validators';
import { DictValuesPipe } from '../../../../shared/pipes/dict-values.pipe';

@Component({
    selector: 'jhi-form-address',
    templateUrl: 'jhi-form-address.template.html'
})
export class FormAddressComponent implements OnInit, OnDestroy {
    @Input()
    address: AddressState;
    @Input()
    fullCountryList: DictionaryCountryState;

    countries: Country[] = null;

    private postalCodeSubscription: Subscription;

    formGroup: FormGroup;

    private cityControl: AbstractControl;

    filteredOptions: Observable<DictionaryCountryState[]>;

    private _isString: any = require('lodash/isString');
    private _isEmpty: any = require('lodash/isEmpty');

    formErrors = {
        street: '',
        buildingNumber: '',
        flatNumber: '',
        city: '',
        postalCode: '',
        country: ''
    };

    constructor(private formBuilder: FormBuilder, private formErrorService: FormErrorService, private dictValuesPipe: DictValuesPipe) {}

    ngOnInit() {
        this.mapDictionary();
        this.buildForm();
        this.formErrorService.touchAllControls(this.formGroup);
        this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false);
        this.setupCountryObserver();

        this.formErrorService.touchAllControls(this.formGroup);
        this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false);

        this.formGroup.valueChanges.subscribe(
            () => (this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false))
        );
    }

    buildForm(): void {
        this.formGroup = this.formBuilder.group({
            street: [{ value: !!this.address ? this.address.street : '', disabled: false }, [Validators.maxLength(50)]],
            buildingNumber: [
                {
                    value: !!this.address ? this.address.buildingNumber : '',
                    disabled: false
                },
                [this.getValidators(10, false, false)]
            ],
            flatNumber: [{ value: !!this.address ? this.address.flatNumber : '', disabled: false }, [Validators.maxLength(10)]],
            city: [{ value: !!this.address ? this.address.city : '', disabled: false }, [this.getValidators(50, false, false)]],
            postalCode: [{ value: !!this.address ? this.address.postalCode : '', disabled: false }, [this.getValidators(6, false, false)]],
            country: [
                {
                    value: !!this.address ? this.address.country : '',
                    disabled: false
                },
                [JhiValidators.autocomplete(this.countries), this.getValidators(50, false, false)]
            ]
        });

        this.cityControl = this.formGroup.controls.city;

        this.postalCodeSubscription = this.formGroup
            .get('postalCode')
            .valueChanges.pipe(
                distinctUntilChanged(),
                pairwise()
            )
            .subscribe(this.postalCodeChanged);
    }

    getValidators(maxLenValue: number, alphaDashCheck: boolean, postalCodeCheck: boolean): ValidatorFn {
        const validators: ValidatorFn[] = [];
        validators.push(Validators.required);
        if (maxLenValue) {
            validators.push(Validators.maxLength(maxLenValue));
        }
        if (alphaDashCheck) {
            validators.push(JhiValidators.alphaAndDash);
        }
        if (postalCodeCheck) {
            validators.push(JhiValidators.zipCode);
        }

        return Validators.compose(validators);
    }

    mapDictionary() {
        this.countries = this.dictValuesPipe.transform(this.fullCountryList.countries);
    }

    setupCountryObserver(): void {
        let initialCountryValue: DictionaryCountryState = { code: null, name: '' };
        if (!!this.address && !this._isEmpty(this.address.country)) {
            const foundCountry: any = this.countries.find(country => this.address.country === country.code);
            if (!this._isEmpty(foundCountry)) {
                initialCountryValue = foundCountry;
            }
        }
        this.filteredOptions = this.formGroup.controls.country.valueChanges.pipe(
            startWith(initialCountryValue),
            map((value: any) => {
                if (!value) {
                    return;
                }
                let filteringValue: DictionaryCountryState = value;
                if (this._isString(value)) {
                    filteringValue = { code: 'YY', name: value };
                }
                return this.filter(filteringValue);
            })
        );
    }

    private filter(value: DictionaryCountryState): DictionaryCountryState[] {
        return this.countries.filter((option: DictionaryCountryState) => {
            return option.name.toLowerCase().includes(value.name.toLowerCase());
        });
    }

    displayFn(item: DictionaryCountryState | string): string {
        if (typeof item === 'string') {
            const selectedCountry: DictionaryCountryState = this.countries.find(option => option.code === item);
            if (selectedCountry && selectedCountry.name) {
                return selectedCountry.name;
            } else {
                return '';
            }
        }
        return item ? item.name : '';
    }

    countrySelected(change: MatAutocompleteSelectedEvent): void {
        this.formGroup.controls.country.patchValue(change.option.value.code);
        this.formGroup.controls.country.updateValueAndValidity();
    }

    postalCodeChanged = ([previous, current]: string[]): void => {
        if (current.length === 2 && previous.length === 1) {
            this.formGroup.get('postalCode').setValue(current + '-');
        }
    };

    isFormValid() {
        return this.formErrorService.isValid(this.formErrors);
    }

    ngOnDestroy(): void {
        this.postalCodeSubscription.unsubscribe();
    }
}
