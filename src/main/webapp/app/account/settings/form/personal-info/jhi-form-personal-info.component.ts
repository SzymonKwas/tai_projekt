import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { JhiValidators } from '../../../../shared/services/validators';
import { PersonState } from '../../../../store/states/person.state';
import { SexEnum } from '../../../../enums/sex-enum';
import { FormErrorService } from '../../../../shared/services/form-error.service';
import { MatDatepicker } from '@angular/material';

@Component({
    selector: 'jhi-form-personal-info',
    templateUrl: 'jhi-form-personal-info.template.html'
})
export class FormPersonalInfoComponent implements OnInit, OnDestroy {
    @Input()
    personState: PersonState;

    formGroup: FormGroup;

    availableSex = [{ name: 'Male', id: SexEnum.MALE }, { name: 'Female', id: SexEnum.FEMALE }];

    firstName: AbstractControl;
    middleName: AbstractControl;
    lastName: AbstractControl;
    birthDate: AbstractControl;
    sex: AbstractControl;
    pesel: AbstractControl;

    formErrors: any = {
        firstName: '',
        middleName: '',
        lastName: '',
        birthDate: '',
        sex: '',
        pesel: ''
    };

    maxDate: Date = new Date();

    private _isString: any = require('lodash/isString');
    private _isEmpty = require('lodash/isEmpty');

    private subscription: Subscription;

    constructor(private formBuilder: FormBuilder, private formErrorService: FormErrorService) {}

    ngOnInit(): void {
        const birthDate = this._isEmpty(this.personState.birthDate)
            ? this.personState.birthDate
            : moment(this.personState.birthDate, 'YYYY-MM-DD');

        this.formGroup = this.formBuilder.group({
            firstName: [
                {
                    value: !!this.personState ? this.personState.firstName : '',
                    disabled: false
                },
                [Validators.required, Validators.maxLength(100)]
            ],
            middleName: [
                {
                    value: !!this.personState ? this.personState.middleName : '',
                    disabled: false
                },
                [Validators.maxLength(100)]
            ],
            lastName: [
                {
                    value: !!this.personState ? this.personState.lastName : '',
                    disabled: false
                },
                [Validators.required, Validators.maxLength(100)]
            ],
            sex: [
                {
                    value: !!this.personState ? this.personState.sex : null,
                    disabled: false
                },
                Validators.required
            ],
            birthDate: [
                {
                    value: birthDate,
                    disabled: false
                },
                Validators.compose([Validators.required, Validators.maxLength(10)])
            ],
            pesel: [
                {
                    value: !!this.personState ? this.personState.pesel : '',
                    disabled: false
                },
                Validators.compose([Validators.required, Validators.maxLength(11), JhiValidators.pesel])
            ]
        });

        this.firstName = this.formGroup.controls['firstName'];
        this.middleName = this.formGroup.controls['middleName'];
        this.lastName = this.formGroup.controls['lastName'];
        this.birthDate = this.formGroup.controls['birthDate'];
        this.sex = this.formGroup.controls['sex'];
        this.pesel = this.formGroup.controls['pesel'];

        this.formErrorService.touchAllControls(this.formGroup);
        this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false);

        this.formGroup.valueChanges.subscribe(
            () => (this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false))
        );
    }

    setSex(id) {
        this.sex.setValue(id);
    }

    isFormValid() {
        return this.formErrorService.isValid(this.formErrors);
    }

    onDateInput(event: KeyboardEvent, matDatepicker: MatDatepicker<any>) {
        switch (event.key) {
            case 'Escape':
            case 'Enter':
                matDatepicker.close();
                break;
            case 'Delete':
            case 'Backspace':
                matDatepicker.select(null);
                matDatepicker.close();
                break;
            case 'Tab':
                break;
            default:
                event.preventDefault();
        }
    }

    clearDateInput() {
        this.birthDate.patchValue('');
        this.formErrors = this.formErrorService.validateForm(this.formGroup, this.formErrors, false);
    }

    ngOnDestroy(): void {}
}
