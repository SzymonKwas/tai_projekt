import { Component, OnInit, ViewChild } from '@angular/core';

import { AccountService, Principal } from 'app/core';
import { PersonState } from '../../store/states/person.state';
import { FormPersonalInfoComponent } from './form/personal-info/jhi-form-personal-info.component';
import { AppState } from '../../store/app.state';
import { State, Store } from '@ngrx/store';
import { AddUserAction } from '../../store/actions/user.actions';
import { FormAddressComponent } from './form/address/jhi-form-address.component';
import { GetCountriesAction } from '../../store/actions/dictionary.actions';
import { distinctUntilChanged } from 'rxjs/internal/operators';
import { DictionaryCountryState } from '../../store/states/dictionary.state';
import { Observable } from 'rxjs/index';
import { AddressState } from '../../store/states/address.state';

@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
    error: string;
    success: string;
    settingsAccount: any;
    languages: any[];

    countriesDictionary$: Observable<DictionaryCountryState>;

    @ViewChild(FormPersonalInfoComponent)
    formPersonalInfoComponent: FormPersonalInfoComponent;

    @ViewChild(FormAddressComponent)
    formAddressComponent: FormAddressComponent;

    person: PersonState;
    address: AddressState;

    constructor(
        private account: AccountService,
        private principal: Principal,
        private state$: State<AppState>,
        private store$: Store<AppState>
    ) {}

    ngOnInit() {
        this.store$.dispatch(new GetCountriesAction());
        this.countriesDictionary$ = this.store$.select(state => state.dictionary.countries).pipe(distinctUntilChanged());

        this.person = this.state$.getValue().user.person;
        this.address = this.person.address;

        this.principal.identity().then(account => {
            this.settingsAccount = this.copyAccount(account);
        });
    }

    save() {
        this.person = this.state$.getValue().user.person;
        this.address = this.person.address;

        const person: PersonState = {
            ...this.formPersonalInfoComponent.formGroup.value,
            id: this.getValue(this.person.id)
        };
        person.address = {
            ...this.formAddressComponent.formGroup.value,
            id: !!this.address ? this.getValue(this.address.id) : null
        };

        const user = { ...this.settingsAccount, person };
        this.account.save(user).subscribe(
            () => {
                this.error = null;
                this.success = 'OK';
                this.principal.identity(true).then(account => {
                    this.store$.dispatch(new AddUserAction(this.principal.createUserFromAccount(account)));
                    this.settingsAccount = this.copyAccount(account);
                });
            },
            () => {
                this.success = null;
                this.error = 'ERROR';
            }
        );
    }

    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl
        };
    }

    getValue(value: any) {
        return !!value ? value : null;
    }
}
