import { Component, OnInit, AfterViewInit, Renderer, ElementRef, Inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from 'app/shared';
import { LoginModalService } from 'app/core';
import { Register } from './register.service';
import { DOCUMENT } from '@angular/common';
declare const grecaptcha: any;

@Component({
    selector: 'jhi-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    captchaError = false;
    registerAccount: any;
    success: boolean;
    modalRef: NgbModalRef;

    constructor(
        private loginModalService: LoginModalService,
        private registerService: Register,
        private elementRef: ElementRef,
        private renderer: Renderer,
        @Inject(DOCUMENT) private document
    ) {}

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#login'), 'focus', []);

        const s = this.document.createElement('script');
        s.type = 'text/javascript';
        s.src = 'https://www.google.com/recaptcha/api.js';
        this.elementRef.nativeElement.appendChild(s);
    }

    register() {
        const responseCaptcha = grecaptcha.getResponse();
        if (responseCaptcha.length === 0) {
            this.captchaError = true;
            return;
        }
        if (this.registerAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.error = null;
            this.errorUserExists = null;
            this.errorEmailExists = null;
            this.registerAccount.langKey = 'en';
            this.registerAccount.recaptchaResponse = responseCaptcha;
            this.registerService.save(this.registerAccount).subscribe(
                () => {
                    this.success = true;
                    this.captchaError = false;
                },
                response => {
                    grecaptcha.reset();
                    return this.processError(response);
                }
            );
        }
    }

    openLogin() {
        this.modalRef = this.loginModalService.open();
    }

    private processError(response: HttpErrorResponse) {
        this.success = null;
        if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
            this.errorUserExists = 'ERROR';
        } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
            this.errorEmailExists = 'ERROR';
        } else {
            this.error = 'ERROR';
        }
    }
}
