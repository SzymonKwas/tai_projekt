import { Flight } from './flight.model';
import { TicketTypeEnum } from '../../enums/ticket-type.enum';
import { TicketClassEnum } from '../../enums/ticket-class.enum';
export interface Ticket {
    id: number;
    flight: Flight;
    countOfLeftTickets: number;
}

export interface BookTicket {
    flight: Flight;
    ticketType: TicketTypeEnum;
    ticketClass: TicketClassEnum;
    firstName: string;
    lastName: string;
    price: number;
}
