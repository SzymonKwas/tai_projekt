export interface Address {
    id: number;
    country: string;
    postalCode: string;
    city: string;
    street: string;
    buildingNumber: string;
    flatNumber: string;
}
