export interface Search {
    fromDate: string;
    toDate: string;
    fromPlace: string;
    toPlace: string;
}
