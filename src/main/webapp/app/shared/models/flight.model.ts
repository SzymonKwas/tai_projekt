export interface Flight {
    id: number;
    fromCity: string;
    toCity: string;
    date: string;
    price: number;
}
