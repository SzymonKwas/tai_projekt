import { SexEnum } from '../../enums/sex-enum';
import { AddressState } from '../../store/states/address.state';

export interface Person {
    id: number;
    address: AddressState;
    sex: SexEnum;
    lastName: string;
    firstName: string;
    middleName: string;
    birthDate: string;
    pesel: string;
    phoneNumber: string;
    email: string;
}
