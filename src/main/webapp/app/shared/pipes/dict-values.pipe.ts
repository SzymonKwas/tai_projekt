import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'dictValues' })
export class DictValuesPipe implements PipeTransform {
    transform(value: any, args: any[] = null): any {
        return Object.keys(value).map(key => {
            return {
                code: key,
                name: value[key]
            };
        });
    }
}
