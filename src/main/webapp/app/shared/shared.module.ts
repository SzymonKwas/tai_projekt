import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { TaiProjektSharedLibsModule, TaiProjektSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatSelectModule,
    MatRadioModule,
    MatButtonToggleModule,
    MatInputModule,
    MatTooltipModule,
    MatIconModule,
    MatSliderModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    MatButtonModule
} from '@angular/material';
import { DictValuesPipe } from './pipes/dict-values.pipe';
import { HasNotAnyAuthorityDirective } from './auth/has-not-any-authority.directive';

@NgModule({
    imports: [
        TaiProjektSharedLibsModule,
        TaiProjektSharedCommonModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'always' }),
        MatInputModule,
        MatTooltipModule,
        MatRadioModule,
        MatButtonToggleModule,
        MatSelectModule,
        MatIconModule,
        MatSliderModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatDialogModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatButtonModule
    ],
    declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective, DictValuesPipe, HasNotAnyAuthorityDirective],
    providers: [DictValuesPipe, { provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        TaiProjektSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        HasNotAnyAuthorityDirective,
        FlexLayoutModule,
        DictValuesPipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TaiProjektSharedModule {}
