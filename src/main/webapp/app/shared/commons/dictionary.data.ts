export interface Dictionary<T = any> {
    [key: string]: T;
}

export interface DictionaryElement {
    code: string;
    name: string;
}

export interface DictionaryCity extends Dictionary {}
export interface DictionaryCountry extends Dictionary {}
