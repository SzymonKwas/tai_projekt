import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { GenericConfirmationData } from './generic-confirmation-data.model';

@Component({
    selector: 'jhi-generic-confirmation-dialog',
    templateUrl: './generic-confirmation.template.html',
    styleUrls: ['./generic-confirmation.scss']
})
export class GenericConfirmationComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public data: GenericConfirmationData) {}
}
