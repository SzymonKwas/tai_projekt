export interface GenericConfirmationData {
    header: string;
    content: string;
    successButton: string;
}
