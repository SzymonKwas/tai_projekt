import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatDialogModule, MatToolbarModule } from '@angular/material';
import { GenericConfirmationComponent } from './generic-confirmation.component';

@NgModule({
    imports: [CommonModule, FlexLayoutModule, MatDialogModule, MatToolbarModule, MatButtonModule],
    exports: [GenericConfirmationComponent],
    declarations: [GenericConfirmationComponent],
    providers: [],
    entryComponents: [GenericConfirmationComponent]
})
export class GenericConfirmationModule {}
