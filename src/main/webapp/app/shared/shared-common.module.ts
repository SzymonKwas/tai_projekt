import { NgModule } from '@angular/core';

import { TaiProjektSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [TaiProjektSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [TaiProjektSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class TaiProjektSharedCommonModule {}
