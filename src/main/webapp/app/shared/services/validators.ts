import { Injectable } from '@angular/core';
import { ValidationErrors, FormControl } from '@angular/forms';
import { PeselService } from './pesel.service';
import { EmailService } from './email.service';
import { PhoneService } from './phone.service';
import { DatepickerService } from './datepicker.service';
import { Country } from '../../store/states/dictionary.state';

@Injectable()
export class JhiValidators {
    private static alphaAndDashRegex: RegExp = /^([a-ząćęłńóśźż]+(-[a-ząćęłńóśźż]*)?)$/i;
    private static alphaRegex: RegExp = /^([a-ząćęłńóśźż]+)$/i;
    private static numericRegex: RegExp = /^([0-9]+)$/;
    private static zipCodeRegex: RegExp = /^([0-9]{2}-[0-9]{3})$/;
    private static floatRegex: RegExp = /([,.])/;

    private static _isNumber: any = require('lodash/isNumber');

    static pesel(control: FormControl): ValidationErrors {
        if (control.value && control.value.length > 0 && !new PeselService().isValidPesel(control.value)) {
            return { pesel: false };
        }
        return null;
    }

    static email(control: FormControl): ValidationErrors {
        if (control.value && control.value.length > 0 && !new EmailService().emailRegexp.test(control.value.trim())) {
            return { email: false };
        } else {
            return null;
        }
    }

    static phone(control: FormControl): ValidationErrors {
        if (control.value && control.value.length > 0 && !new PhoneService().isValid(control.value)) {
            return { phone: false };
        } else {
            return null;
        }
    }

    static datepicker(control: FormControl): ValidationErrors {
        if (control.value && !new DatepickerService().isValid(control.value)) {
            return { datepicker: false };
        } else {
            return null;
        }
    }

    static minimalValue = (minimalValue: number) => {
        return control => {
            if (control.value && parseInt(control.value, 10) < minimalValue) {
                return { minimalValue: false };
            } else {
                return null;
            }
        };
    };

    static maximumValue = (maximumValue: number) => {
        return control => {
            if (control.value && parseInt(control.value, 10) > maximumValue) {
                return { maximumValue: false };
            } else {
                return null;
            }
        };
    };

    static float(control: FormControl): ValidationErrors {
        let val: string = control.value;
        if (control.value && typeof control.value === 'number') {
            val = control.value.toString();
        }
        if (val && val.length > 0 && JhiValidators.floatRegex.test(val)) {
            return { float: false };
        } else {
            return null;
        }
    }

    static alphaAndDash(control: FormControl): ValidationErrors {
        if (control.value && control.value.length > 0 && !JhiValidators.alphaAndDashRegex.test(control.value)) {
            return { alphaAndDash: false };
        } else {
            return null;
        }
    }

    static alpha(control: FormControl): ValidationErrors {
        if (control.value && control.value.length > 0 && !JhiValidators.alphaRegex.test(control.value)) {
            return { alpha: false };
        } else {
            return null;
        }
    }

    static numeric(control: FormControl): ValidationErrors {
        if (control.value && control.value.length > 0 && !JhiValidators.numericRegex.test(control.value)) {
            return { numeric: false };
        } else {
            return null;
        }
    }

    static zipCode(control: FormControl): ValidationErrors {
        if (control.value && control.value.length > 0 && !JhiValidators.zipCodeRegex.test(control.value)) {
            return { zipCode: false };
        } else {
            return null;
        }
    }

    static autocomplete = (countries: Country[]) => {
        return control => {
            if (control.value && control.value.length > 0 && !countries.find((country: Country) => country.code === control.value)) {
                return { autocomplete: false };
            } else {
                return null;
            }
        };
    };

    constructor() {}
}
