import { Injectable } from '@angular/core';

@Injectable()
export class PeselService {
    constructor() {}

    isValidPesel = (pesel: string): boolean => {
        const reg = /^[0-9]{11}$/;
        if (!reg.test(pesel)) {
            return false;
        } else {
            const digits = ('' + pesel).split('');
            if (parseInt(pesel.substring(4, 6), 10) > 31) {
                return false;
            }
            let checksum =
                (1 * parseInt(digits[0], 10) +
                    3 * parseInt(digits[1], 10) +
                    7 * parseInt(digits[2], 10) +
                    9 * parseInt(digits[3], 10) +
                    1 * parseInt(digits[4], 10) +
                    3 * parseInt(digits[5], 10) +
                    7 * parseInt(digits[6], 10) +
                    9 * parseInt(digits[7], 10) +
                    1 * parseInt(digits[8], 10) +
                    3 * parseInt(digits[9], 10)) %
                10;
            if (checksum === 0) {
                checksum = 10;
            }
            checksum = 10 - checksum;
            return parseInt(digits[10], 10) === checksum;
        }
    };

    extractDateInfoFromPesel(pesel: string): any {
        const yearPeselPartial: number = parseInt(pesel.slice(0, 2), 10);
        const monthPeselPartial: number = parseInt(pesel.slice(2, 4), 10);
        const dayPeselPartial: string = pesel.slice(4, 6);
        let yearPrefix: string;
        let extractedYear: string;
        let extractedMonth: string;

        if (monthPeselPartial > 0 && monthPeselPartial < 20) {
            yearPrefix = '19';
            extractedMonth = monthPeselPartial.toString();
        } else if (monthPeselPartial > 20 && monthPeselPartial < 40) {
            yearPrefix = '20';
            extractedMonth = (monthPeselPartial - 20).toString();
        } else if (monthPeselPartial > 40 && monthPeselPartial < 60) {
            yearPrefix = '21';
            extractedMonth = (monthPeselPartial - 40).toString();
        } else if (monthPeselPartial > 60 && monthPeselPartial < 80) {
            yearPrefix = '22';
            extractedMonth = (monthPeselPartial - 60).toString();
        } else if (monthPeselPartial > 80 && monthPeselPartial < 100) {
            yearPrefix = '18';
            extractedMonth = (monthPeselPartial - 80).toString();
        }

        if (extractedMonth.length === 1) {
            extractedMonth = '0' + extractedMonth;
        }
        extractedYear = yearPeselPartial.toString().length === 1 ? '0' + yearPeselPartial.toString() : yearPeselPartial.toString();

        return {
            day: dayPeselPartial,
            month: extractedMonth,
            year: yearPrefix + extractedYear
        };
    }
}
