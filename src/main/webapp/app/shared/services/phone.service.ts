import { Injectable } from '@angular/core';

@Injectable()
export class PhoneService {
    constructor() {}

    isValid = (value: string): boolean => {
        return /^[+]\d{11}$|^\d{9}$/.test(value);
    };

    requiredDigitNumber = (value: string): number => {
        return this.hasPlusPrefix(value) ? 11 : 9;
    };

    requiredCharNumber = (value: string): number => {
        return this.hasPlusPrefix(value) ? 12 : 9;
    };

    hasPlusPrefix = (value: string): boolean => {
        return /^[+].*$/.test(value);
    };
}
