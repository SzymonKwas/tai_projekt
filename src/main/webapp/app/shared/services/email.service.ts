import { Injectable } from '@angular/core';

@Injectable()
export class EmailService {
    readonly emailRegexp = new RegExp(
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+" +
            '@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])' +
            '|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$'
    );

    constructor() {}

    isValid = (value: string): boolean => {
        return this.emailRegexp.test(value.trim());
    };
}
