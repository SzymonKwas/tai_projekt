import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { DictionaryCity, DictionaryCountry } from '../commons/dictionary.data';
@Injectable({
    providedIn: 'root'
})
export class DictionaryService {
    constructor(private http: HttpClient) {}

    getCitiesDictionary(): Observable<DictionaryCity> {
        return this.http.get<DictionaryCity>(`/api/dictionary/cities`);
    }

    getCountriesDictionary(): Observable<DictionaryCountry> {
        return this.http.get<DictionaryCountry>(`/api/dictionary/countries`);
    }
}
