import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs/index';
import { Search } from '../models/search.mode';
import { Flight } from '../models/flight.model';
import { delay } from 'rxjs/internal/operators';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    constructor(private http: HttpClient) {}

    searchFlights(search: Search): Observable<Flight[]> {
        if (environment.mockFlights) {
            return this.mockFlights();
        } else {
            return this.http.post<Flight[]>(`/api/flight/search`, search);
        }
    }

    private mockFlights(): Observable<Flight[]> {
        return of([
            {
                id: 1,
                fromCity: 'KRK',
                toCity: 'WAW',
                date: '2019-01-12:00:00',
                price: 120
            },
            {
                id: 2,
                fromCity: 'KRK',
                toCity: 'WAW',
                date: '2019-01-12:00:00',
                price: 170
            },
            {
                id: 3,
                fromCity: 'KRK',
                toCity: 'WAW',
                date: '2019-01-11:06:40',
                price: 110
            },
            {
                id: 4,
                fromCity: 'KRK',
                toCity: 'WAW',
                date: '2019-01-14:05:30',
                price: 110
            },
            {
                id: 5,
                fromCity: 'KRK',
                toCity: 'WAW',
                date: '2019-01-14:10:00',
                price: 620
            }
        ]).pipe(delay(1000));
    }
}
