import { Injectable } from '@angular/core';
import * as moment from 'moment';

interface MomentDateValueObject {
    year: number;
    month: number;
    date: number;
}

@Injectable()
export class DatepickerService {
    public readonly dateRegex = /\d{4}-\d{2}-\d{2}(T\d{2}:\d{2}:\d{2}.\d{2,}Z)?/i;
    private _isString: any = require('lodash/isString');
    private _isObject: any = require('lodash/isObject');

    isValid(value: any): boolean {
        let stringDateValue: string;

        if (this._isObject(value) && value._isAMomentObject) {
            stringDateValue = this.stringifyDatepickerMomentValue(value);
        } else if (this._isString(value)) {
            stringDateValue = value;
        } else {
            return true;
        }

        return this.dateRegex.test(stringDateValue);
    }

    stringifyDatepickerMomentValue(value: any) {
        if (this._isString(value._i)) {
            return value._i;
        } else {
            const valueMomentRepresentation: MomentDateValueObject = value._i as MomentDateValueObject;
            return moment(
                `${valueMomentRepresentation.year}-${valueMomentRepresentation.month}-${valueMomentRepresentation.date}`,
                'YYYY-MM-DD'
            ).format('YYYY-MM-DD');
        }
    }
}
