import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

@Injectable()
export class FormErrorService {
    validationMessages = {
        required: 'Field is mandatory',
        email: 'Incorrect adres email',
        minlength: 'Too few characters',
        maxlength: 'too many characters',
        pesel: 'Incorrect PESEL number PESEL',
        phone: 'Incorrect number telefonu',
        datepicker: 'Incorrect format (RRRR-MM-DD)',
        matDatepickerParse: 'Illegal characters. The correct format (YYYY-MM-DD)',
        matDatepickerMin: 'Too early date',
        pattern: 'Incorrect format',
        minimalValue: 'Too low value',
        maximumValue: 'Too high value',
        min: 'Too low value',
        max: 'Too high value',
        zipCode: 'The field should be in the format 00-000',
        autocomplete: 'Must be selected from list'
    };

    private _includes = require('lodash/includes');
    private _forEach = require('lodash/forEach');
    private _keys = require('lodash/keys');

    touchAllControls(formGroup: FormGroup): void {
        for (const field in formGroup.controls) {
            if (formGroup.controls.hasOwnProperty(field)) {
                const control = formGroup.get(field);
                if ((control as FormGroup).controls) {
                    this.touchAllControls(control as FormGroup);
                }
                control.updateValueAndValidity();
                control.markAsDirty();
                control.markAsTouched();
            }
        }
    }

    validateForm(formToValidate: FormGroup | FormArray, formErrors: any, checkDirty?: boolean, errorPreference?: string): any {
        const form = formToValidate;

        for (const field in formErrors) {
            if (formErrors.hasOwnProperty(field)) {
                const control = form.get(field);
                const oldformErrorValue = formErrors[field];
                formErrors[field] = '';
                if (control) {
                    if (!!(control as FormGroup).controls) {
                        formErrors[field] = oldformErrorValue;
                        continue;
                    }

                    if (!control.valid) {
                        if (!checkDirty || (control.dirty || control.touched)) {
                            const keys: string[] = this._keys(control.errors);
                            if (keys.length) {
                                if (errorPreference && this._includes(keys, errorPreference)) {
                                    formErrors[field] = formErrors[field] || this.validationMessages[errorPreference];
                                } else {
                                    keys.forEach((key: string) => (formErrors[field] = formErrors[field] || this.validationMessages[key]));
                                }
                            }
                        }
                    }
                }
            }
        }

        return formErrors;
    }

    isValid(formErrors: any): boolean {
        for (const field in formErrors) {
            if (field) {
                if (formErrors[field] !== '') {
                    return false;
                }
            }
        }

        return true;
    }
}
